# About
`gorge` is a commandline tool for mananging a [Gorgeous](https://gitlab.com/Plasticity/gorgeous) project.

# Installation

1. To install, simply run:

    `curl https://gitlab.com/Plasticity/gorge/raw/master/install.sh | bash`

    Once you have successfully installed `gorge`, running `gorge version` should yield the current installed version.

2. While `gorge` is successfully installed at this point, building and running an actual Gorgeous project may require some further dependencies. `gorge` helps install these dependencies. Currently `macOS` (with `brew`), `Ubuntu 16.04` (with `apt-get`), and `Debian Stretch` (with `apt-get`).

    To install these dependencies (may require `sudo` on Linux), run:
    `gorge install-dependencies`

# Usage
1. Create a folder where you want to create a Gorgeous project: `mkdir helloworld`
2. Change directory into that folder: `cd helloworld`
3. Run `gorge setup` to create a starter Gorgeous project in that folder
4. Run `gorge run` to run the starter project in that folder

# Other usage
`gorge` has other functions for building, running, updating Gorgeous in a project, self-updating `gorge`, and installing system dependencies. Refer to `gorge help` for more information.

# Developing and Deployment
This repository has Gitlab CI setup to automatically deploy the latest pre-built Docker image of gorge to `registry.gitlab.com/plasticity/gorge:latest` on push. 

Simply edit the `gorge` source, commit, and push to have changes deployed. Run `gorge update` on any machines using gorge to pull the latest version of `gorge`.


__Note__: If you edit `gorge install-dependencies`, pre-built Docker images using `gorge` will not have those changes reflected until the image is re-built.