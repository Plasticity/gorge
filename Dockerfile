FROM gcc:6.2.0

# Install dependencies and setup
RUN cp  /etc/apt/sources.list /etc/apt/sources.list.bak
RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get install curl -y
RUN apt-get install python -y
RUN curl https://gitlab.com/Plasticity/gorge/raw/master/install.sh | bash

############# IF USING GCC #####################
# Note: This updates the package manager to use Debian packages from 'stretch' instead of 'jessie' 
# because the newer ones are required for gorge
RUN cp /etc/apt/sources.list.bak /etc/apt/sources.list
RUN sed -i 's/jessie/stretch/g' /etc/apt/sources.list
RUN apt-get update
RUN gorge install-dependencies
RUN sed -i 's/stretch/jessie/g' /etc/apt/sources.list
RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list
RUN gorge install-dependencies
RUN apt-get update

######## IF USING UBUNTU 16.04 #################
# Note: this will not install GCC since it we are using the gcc:6.2.0 image which already has GCC installed.
# This is purposely done to prevent compiling and installing GCC from source on every build which is incredibly slow.
# If you want to test if the install-dependencies install for GCC works, you have to use the ubuntu:16.04 image which does not have the 
# latest version of GCC pre-installed.
# RUN gorge install-dependencies

CMD gorge update